<?php
require __DIR__.'/framework/F3/base.php';
require_once 'AWSSDKforPHP/sdk.class.php';
 
//F3::set('CACHE',TRUE);
F3::set('DEBUG',3);
F3::set('UI','template/');
F3::set('POST',F3::scrub($_POST));
F3::set('DB',
	new DB(
		'mysql:host=localhost;port=3306;dbname=qcrt',
		'root',
		'myers478'
	)
);

$subdomain_code = strtok($_SERVER["HTTP_HOST"], ".");
F3::set('subdomain', $subdomain_code!='www' ? $subdomain_code : 'qcrt');
F3::set('URL_BASE', 'http://'.$subdomain_code.'.qcrt.com');
	
//--- AppFramework ---

	// Home Page
	F3::call('controller/public.php');
	F3::route('GET /', 'front->home');
	F3::route('GET /how-it-works', 'front->about');
	F3::route('GET /privacy-policy', 'front->privacy');
	F3::route('GET /legal-notices', 'front->legal');
	
	//Disable Public Account Signup
	F3::route('GET /account', 'front->account');
	F3::route('POST /account', 'front->account');
	F3::route('POST /account/check-email','front->checkUserEmail');
	
	//Admin Routes
	F3::call('controller/admin.php');
	F3::route('GET /admin','admin->home');
	F3::route('GET /admin/forgot','admin->forgot');
	F3::route('GET /admin/account','admin->account');
	F3::route('GET /admin/users','admin->users');
	F3::route('GET /admin/users/new','admin->new_user');
	F3::route('GET /admin/users/edit/@id','admin->edit_user');
	F3::route('GET /admin/login', 'admin->login');
	F3::route('GET /admin/logout','admin->logout');
	F3::route('GET /admin/activate','admin->activate');

	F3::route('POST /admin/users/check-new-email','admin->checkNewUserEmail');
	F3::route('POST /admin/users/check-edit-email','admin->checkEditUserEmail');
	F3::route('POST /admin/login','admin->login');
	F3::route('POST /admin/account','admin->update');
	F3::route('POST /admin/forgot','admin->forgot');
	F3::route('POST /admin/users/new','admin->new_user');
	F3::route('POST /admin/users/edit','admin->edit_user');
	F3::route('POST /admin/users/delete','admin->delete_user');
	F3::route('POST /admin/users/status','admin->change_status');
	
	//Admin Store Routes
	F3::call('controller/admin/store.php');
	F3::route('GET /admin/stores','admin_store->view');
	F3::route('GET /admin/stores/new','admin_store->create');
	F3::route('GET /admin/stores/edit/@id','admin_store->update');
	F3::route('GET /admin/stores/delete','admin_store->delete');
	
	F3::route('POST /admin/stores/new','admin_store->create');
	F3::route('POST /admin/stores/edit','admin_store->update');
	F3::route('POST /admin/stores/delete','admin_store->delete');
	F3::route('POST /admin/stores/status','admin_store->status');
	
	//Admin Window Routes
	F3::call('controller/admin/window.php');
	F3::route('GET /admin/windows','admin_window->view');
	F3::route('GET /admin/windows/@id','admin_window->view');
	F3::route('GET /admin/windows/new','admin_window->create');
	F3::route('GET /admin/windows/new/@id','admin_window->create');
	F3::route('GET /admin/windows/edit/@id','admin_window->update');
	F3::route('GET /admin/windows/edit/@id/@sid','admin_window->update');
	F3::route('GET /admin/windows/delete','admin_window->delete');
	F3::route('GET /admin/windows/uploadCallback/*','admin_window->uploadCallback');
	
	F3::route('POST /admin/windows/new','admin_window->create');
	F3::route('POST /admin/windows/edit','admin_window->update');
	F3::route('POST /admin/windows/delete','admin_window->delete');
	F3::route('POST /admin/windows/status','admin_window->status');
	
	//Admin Door Routes
	F3::call('controller/admin/door.php');
	F3::route('GET /admin/doors','admin_door->view');
	F3::route('GET /admin/doors/@id','admin_door->view');
	F3::route('GET /admin/doors/new','admin_door->create');
	F3::route('GET /admin/doors/new/@id','admin_door->create');
	F3::route('GET /admin/doors/edit/@id','admin_door->update');
	F3::route('GET /admin/doors/edit/@id/@sid','admin_door->update');
	F3::route('GET /admin/doors/delete','admin_door->delete');
	
	F3::route('POST /admin/doors/new','admin_door->create');
	F3::route('POST /admin/doors/edit','admin_door->update');
	F3::route('POST /admin/doors/delete','admin_door->delete');
	F3::route('POST /admin/doors/status','admin_door->status');
	
	//User Routes
	F3::call('controller/user.php');
	F3::route('GET /user','user->home');
	F3::route('GET /user/@store_id','user->home');
	F3::route('GET /user/login','user->login');
	F3::route('GET /user/logout','user->logout');
	F3::route('GET /user/forgot','user->forgot');
	F3::route('GET /user/account','user->account');

	F3::route('POST /user/check-email','user->checkUserEmail');
	F3::route('POST /user/login','user->login');
	F3::route('POST /user/account','user->account');
	F3::route('POST /user/forgot','user->forgot');
	
//--- AppFramework ---

 
F3::run();

?>