$('document').ready(function(){
	/* Build the DataTable */
    $('#data_list').dataTable({
    	"bPaginate": false,
		"oLanguage": { "sSearch": '<span class="add-on"><i class="icon-search"></i></span>' },
		"aaSorting": [[ 1, "asc" ]],
		"aoColumnDefs": [
			{ "bSearchable": false, "bVisible": false, "aTargets": [ 1 ] }
        ],
		sDom: 'Tl<"input-prepend"f>rtip>'
    });

	var cnt = $("#data_list_filter label").contents()
	$("#data_list_filter label").replaceWith(cnt);

});