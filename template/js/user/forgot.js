$(document).ready(function(){

	var validator = $("#forgotForm").validate({ 
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false,
        messages: { 
            email: { 
                required: "Enter your email", 
                email: "Enter your email", 
            }, 
        }, 
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() { 
            alert("submitted!"); 
        }, 
        success: function(label) { 
            label.parent().parent().removeClass("error");
        } 
    }); 
    
	$('#forgotButton').click(function() {
			
		if($("#forgotForm").valid()){
			$.ajax({
				url : URL_BASE+'/user/forgot',
				type: 'POST',
	            dataType: 'text',
				data: $('#forgotForm').serialize(),
				beforeSend : function(){
					$('#forgotButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#forgotButton').button('reset');
				},
				success : function(msg) {
					$('#forgotButton').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast');
					window.location = URL_BASE+'/user';
				}
			});
		}
		
		return false;
	});
	
});