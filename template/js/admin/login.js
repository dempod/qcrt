$(document).ready(function(){

	var validator = $("#loginForm").validate({ 
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false,
        messages: { 
            username: { 
                required: "Enter a username",  
            }, 
            password: { 
                required: "Provide a password", 
            }
        }, 
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() { 
            alert("submitted!"); 
        }, 
        success: function(label) { 
            label.parent().parent().removeClass("error");
        } 
    }); 
    
	$('#login').click(function() {
			
		if($("#loginForm").valid()){
			$.ajax({
				url : URL_BASE+'/admin/login',
				type: 'POST',
	            dataType: 'text',
				data: $('#loginForm').serialize(),
				beforeSend : function(){
					$('#login').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					//alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#login').button('reset');
				},
				success : function(msg) {
					$('#login').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast');
					window.location = URL_BASE+'/admin';
				}
			});
		}
		
		return false;
	});
	
});