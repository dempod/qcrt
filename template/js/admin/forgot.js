$(document).ready(function(){

    var validator = $("#forgotForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: true, 
	    onfocusout: false,
      	onkeyup: false,
        rules: { 
            email: { 
                required: true, 
                email: true, 
            }
        }, 
        messages: { 
            email: { 
                required: "Please enter a valid email address", 
                minlength: "Please enter a valid email address",
            }
        }, 
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() { 
            alert("submitted!"); 
        }, 
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 
    
    $('#forgotButton').click(function() {
    
    	$('.alert').hide('fast');
			
		if($("#forgotForm").valid()){
			$.ajax({
				url : URL_BASE+'/admin/forgot',
				type: 'POST',
	            dataType: 'text',
				data: $('#forgotForm').serialize(),
				beforeSend : function(){
					$('#forgotButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					//alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#forgotButton').button('reset');
				},
				success : function(msg) {
					$('#forgotButton').button('reset');
					$('#.error').hide();
					$('.alert-success').show('fast').delay(3000).fadeOut('slow');
				}
			});
		}
		
		return false;
	});
	
});