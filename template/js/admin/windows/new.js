$(document).ready(function(){

    var validator = $("#windowForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false, 
	    onfocusout: false,
      	onkeyup: false,
      	onkeypress: false,
        rules: {}, 
        messages: {},
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() {},
        invalidHandler: function(error, element){},
        showErrors: function (errorMap, errorList) {
        	for (var i = 0; errorList[i]; i++) {
            	var element = this.errorList[i].element;
            	this.errorsFor(element).remove();
        	}
        	this.defaultShowErrors();
    	},
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 

	$('#view_name').change(function(){
		if($(this).attr('value')!=''){
			$('#window_name').val('');
		}
	});
    
    $('#windowButton').click(function() {
		$('#windowButton').button('loading');
    	$('.alert').hide('fast');
			
		if($("#windowForm").valid()){
			if($('#image_upload_flag').val()==1){
				//Start Upload
				swfu.startResizedUpload(swfu.getFile(0).id, swfu.customSettings.thumbnail_width, swfu.customSettings.thumbnail_height, SWFUpload.RESIZE_ENCODING.JPEG, swfu.customSettings.thumbnail_quality);
				swfu.startResizedUpload(swfu.getFile(0).id, 200, 150, SWFUpload.RESIZE_ENCODING.JPEG, 80);
			}else{
				//submit the form
				$('#windowForm').submit();
			}
		}else{
			$('#windowButton').button('reset');
		}
		
		return false;
	});
	
});