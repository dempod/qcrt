$(document).ready(function(){

    var validator = $("#editForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false, 
	    onfocusout: false,
      	onkeyup: false,
      	onkeypress: false,
        rules: {}, 
        messages: {},
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() {},
        invalidHandler: function(error, element){},
        showErrors: function (errorMap, errorList) {
        	for (var i = 0; errorList[i]; i++) {
            	var element = this.errorList[i].element;
            	this.errorsFor(element).remove();
        	}
        	this.defaultShowErrors();
    	},
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 
    
    $('#doorButton').click(function() {
		$('#doorButton').button('loading');
    	$('.alert').hide('fast');
			
		if($("#doorForm").valid()){
			$('#doorForm').submit();
		}else{
			$('#doorButton').button('reset');
		}
		
		return false;
	});
	
});