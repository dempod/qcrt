$(document).ready(function(){
	
    var validator = $("#adminAccountForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false, 
	    onfocusout: false,
      	onkeyup: false,
        rules: { 
            username: { 
                required: true, 
                minlength: 2, 
            }, 
            password: { 
                minlength: 5 
            }, 
            password_confirm: { 
                minlength: 5, 
                equalTo: "#password" 
            }, 
            email: { 
                required: true, 
                email: true, 
            }
        }, 
        messages: { 
            username: { 
                required: "Enter a username", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            password: { 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            password_confirm: { 
                minlength: jQuery.format("Enter at least {0} characters"), 
                equalTo: "Enter the same password as above" 
            }, 
            email: { 
                required: "Please enter a valid email address", 
                minlength: "Please enter a valid email address", 
            }
        }, 
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        },
        showErrors: function (errorMap, errorList) {
        	for (var i = 0; errorList[i]; i++) {
            	var element = this.errorList[i].element;
            	this.errorsFor(element).remove();
        	}
        	this.defaultShowErrors();
    	}, 
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 
    
    $('#accountButton').click(function() {
    
    	$('.alert').hide('fast');
			
		if($("#adminAccountForm").valid()){
			$.ajax({
				url: URL_BASE+'/admin/account',
				type: 'POST',
	            dataType: 'text',
				data: $('#adminAccountForm').serialize(),
				beforeSend : function(){
					$('#accountButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					//alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#accountButton').button('reset');
				},
				success : function(msg) {
					$('#accountButton').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast').delay(1500).fadeOut('slow');
				}
			});
		}
		
		return false;
	});
});