$(document).ready(function(){

    var validator = $("#newForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false, 
	    onfocusout: false,
      	onkeyup: false,
      	onkeypress: false,
        rules: { 
            number: { 
                required: true, 
                minlength: 2, 
            }, 
            name: { 
                required: true, 
                minlength: 2, 
            },

        }, 
        messages: { 
            number: { 
                required: "Enter a Store Number", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            name: { 
                required: "Enter a Store Name", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
        },
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() { 
            //alert("submitted!"); 
        },
        invalidHandler: function(error, element){
        	//element.parent().parent().addClass("error");
        },
        showErrors: function (errorMap, errorList) {
        	for (var i = 0; errorList[i]; i++) {
            	var element = this.errorList[i].element;
            	this.errorsFor(element).remove();
        	}
        	this.defaultShowErrors();
    	},
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 
    
    $('#newButton').click(function() {

    	$('.alert').hide('fast');
			
		if($("#newForm").valid()){
			$('#newButton').button('loading');
			$("#newForm").submit();
			/*
			$.ajax({
				url: URL_BASE+'/admin/stores/new',
				type: 'POST',
	            dataType: 'text',
				data: $('#newForm').serialize(),
				beforeSend : function(){
					$('#newButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					//alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#newButton').button('reset');
				},
				success : function(msg) {	
					$('#newForm')[0].reset();
					$('#newButton').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast').delay(3000).fadeOut('slow');
				}
			});
			*/
		}
		
		return false;
	});
	
});