$(document).ready(function(){	
	
	$('#editUserButton').click(function() {
	    
	    $("#editUserForm").removeData("validator");
	    var validator = $("#editUserForm").validate({
		    errorElement: "span",
		    errorClass: "help-inline",
		    validClass: "success",
		    onsubmit: false, 
		    onfocusout: false,
	      	onkeyup: false,
	      	onkeypress: false,
	        rules: { 
	            firstname: { 
	                required: true, 
	                minlength: 2, 
	            }, 
	            lastname: { 
	                required: true, 
	                minlength: 2, 
	            },
	            password: {
	                minlength: 5 
	            }, 
	            password_confirm: { 
	                minlength: 5, 
	                equalTo: "#password" 
	            }, 
	            email: { 
	                required: true, 
	                email: true,
	                remote: {
	                	url: 'http://www.qcrt.com/admin/users/check-edit-email',
						type: 'POST',
						//dataType: 'text',
						async: false,
						data: $("#editUserForm").serialize(),
	                },
	            }
	        }, 
	        messages: { 
	            firstname: { 
	                required: "Enter a First Name", 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            lastname: { 
	                required: "Enter a Last Name", 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            password: { 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            password_confirm: { 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	                equalTo: "Enter the same password as above" 
	            }, 
	            email: { 
	                required: "Please enter a valid email address", 
	                minlength: "Please enter a valid email address", 
	                remote: jQuery.format("{0} is already in use")
	            }
	        },
	        errorPlacement: function(error, element) { 
				error.appendTo( element.parent() ); 
				element.parent().parent().addClass("error");
	        }, 
	        submitHandler: function() { 
	            //alert("submitted!"); 
	        },
	        invalidHandler: function(error, element){
	        	//element.parent().parent().addClass("error");
	        },
	        showErrors: function (errorMap, errorList) {
	        	for (var i = 0; errorList[i]; i++) {
	            	var element = this.errorList[i].element;
	            	this.errorsFor(element).remove();
	        	}
	        	this.defaultShowErrors();
	    	},
	        success: function(label) { 
	            label.parent().parent().removeClass("error");
	        }
	    }); 

    	$('.alert').hide('fast');
			
		if($("#editUserForm").valid()){
			$("#editUserForm").submit();
			/*
			$.ajax({
				url: URL_BASE+'/admin/users/edit',
				type: 'POST',
	            dataType: 'text',
				data: $('#editUserForm').serialize(),
				beforeSend : function(){
					$('#editUserButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					//alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#editUserButton').button('reset');
				},
				success : function(msg) {
					$('#editUserButton').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast').delay(3000).fadeOut('slow');
				}
			});
			*/
		}
		
		return false;
	});
	
	    //Disable password fields if auto_password is checked
    $('input[type="checkbox"][name="auto_password"]').change(function() {
		if(this.checked) {
			$('input[type="password"][name="password"]').toggle('disabled');
			$('input[type="password"][name="password_confirm"]').toggle('disabled');
		}else{
			$('input[type="password"][name="password"]').toggle('disabled');
			$('input[type="password"][name="password_confirm"]').toggle('disabled');
		}
	});
	
});