$(document).ready(function(){

    var validator = $("#newUserForm").validate({
	    errorElement: "span",
	    errorClass: "help-inline",
	    validClass: "success",
	    onsubmit: false, 
	    onfocusout: false,
      	onkeyup: false,
      	onkeypress: false,
        rules: { 
            firstname: { 
                required: true, 
                minlength: 2, 
            }, 
            lastname: { 
                required: true, 
                minlength: 2, 
            },
            password: {
            	required: function(element){
            		return $("input:checkbox[name='auto_password']:checked").val() != 'auto';
            	}, 
                minlength: 5 
            }, 
            password_confirm: { 
            	required: function(element){
            		return $("input:checkbox[name='auto_password']:checked").val() != 'auto';
            	}, 
                minlength: 5, 
                equalTo: "#password" 
            }, 
            email: { 
                required: true, 
                email: true,
                remote: {
                	url: 'http://bucketfile.com/admin/users/check-new-email',
					type: 'POST',
					async: false,
                },
            }
        }, 
        messages: { 
            firstname: { 
                required: "Enter a First Name", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            lastname: { 
                required: "Enter a Last Name", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            password: { 
            	required: "Enter a Password", 
                minlength: jQuery.format("Enter at least {0} characters"), 
            }, 
            password_confirm: { 
            	required: "Enter a Confirmation Password",
                minlength: jQuery.format("Enter at least {0} characters"), 
                equalTo: "Enter the same password as above" 
            }, 
            email: { 
                required: "Please enter a valid email address", 
                minlength: "Please enter a valid email address", 
                remote: jQuery.format("{0} is already in use")
            }
        },
        errorPlacement: function(error, element) { 
			error.appendTo( element.parent() ); 
			element.parent().parent().addClass("error");
        }, 
        submitHandler: function() { 
            //alert("submitted!"); 
        },
        invalidHandler: function(error, element){
        	//element.parent().parent().addClass("error");
        },
        showErrors: function (errorMap, errorList) {
        	for (var i = 0; errorList[i]; i++) {
            	var element = this.errorList[i].element;
            	this.errorsFor(element).remove();
        	}
        	this.defaultShowErrors();
    	},
        success: function(label) { 
            label.parent().parent().removeClass("error");
        }
    }); 
    
    $('#newUserButton').click(function() {
		
    	$('.alert').hide('fast');
			
		if($("#newUserForm").valid()){
			$("#newUserForm").submit();
		}
		
		return false;
	});
	
	//Disable password fields if auto_password is checked
    $('input[type="checkbox"][name="auto_password"]').change(function() {
		if(this.checked) {
			$('input[type="password"][name="password"]').toggle('disabled');
			$('input[type="password"][name="password_confirm"]').toggle('disabled');
		}else{
			$('input[type="password"][name="password"]').toggle('disabled');
			$('input[type="password"][name="password_confirm"]').toggle('disabled');
		}
	});
	
});