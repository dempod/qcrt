$(document).ready(function(){
	
	//Delete User
	$('.deleteButton').click(function(event) {
		var id = $(this).val();

		if (confirm("Are you sure you want to delete this door?")){
			
			$.ajax({
	    		url : URL_BASE+'/admin/doors/delete',
	    		type: 'POST',
	            dataType: 'text',
	    		data: 'id='+id,
	    		beforeSend : function(){
	    			$('#editButton').button('loading');
	    		},
	    		error : function(xhr, ajaxOptions, thrownError) {
	    			alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
	    			$('#deleteButton').button('reset');
	    		},
	    		success : function(msg) {
	    			$('#row_'+id).fadeOut('slow');
	    			$('#deleteButton').button('reset');
	    		}
	    	});

  		}
		
		event.preventDefault();
	});
	
    /* Build the DataTable */
    $('#data_list').dataTable({
    	"bPaginate": false,
    });
    
    /* Activate/Deactive A Store */
	$('.status').bind('click', function(event){
		var myElem = $(this);
	
		if (myElem.children('input').eq(0).val()==0) {
		   status = '1';
		} else {
		    status = '0';
		}
	
		$.ajax({
    		url : URL_BASE+'/admin/doors/status',
    		type: 'POST',
            dataType: 'text',
    		data: 'id='+$(this).parent().data('door_id')+'&status='+status,
    		beforeSend : function(){},
    		error : function(xhr, ajaxOptions, thrownError) {
    			alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
    			myElem.removeAttr("disabled");
    			if(status=='1'){
	    			myElem.attr('checked', 'checked');
    			}else{
	    			myElem.removeAttr('checked');
    			}
    		},
    		success : function(msg) {
				my_input = myElem.children('input').eq(0);
				my_span = myElem.children('span').eq(0);

				if(my_input.val()==0){
					my_input.eq(0).val(1);
					my_span.html('Active');
					my_span.addClass('label-success')
				}else{
					my_input.eq(0).val(0);
					my_span.html('Inactive');
					my_span.removeClass('label-success');
				}
    		}
    	});

		event.preventDefault();
	});
	
});