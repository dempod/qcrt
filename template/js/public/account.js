$(document).ready(function(){
	
	$('#userAccountButton').click(function() {
	    
	    $("#userAccountForm").removeData("validator");
	    var validator = $("#userAccountForm").validate({
		    errorElement: "span",
		    errorClass: "help-inline",
		    validClass: "success",
		    onsubmit: false, 
		    onfocusout: false,
	      	onkeyup: false,
	      	onkeypress: false,
	        rules: {
	        	firstname: { 
	                required: true, 
	                minlength: 2, 
	            }, 
	            lastname: { 
	                required: true, 
	                minlength: 2, 
	            },
	            password: {
	                minlength: 5,
	                required: true 
	            }, 
	            password_confirm: { 
	                minlength: 5, 
	                equalTo: "#password" 
	            }, 
	            email: { 
	                required: true, 
	                email: true,
	                remote: {
	                	url: 'account/check-email',
						type: 'POST',
						//dataType: 'text',
						async: false,
	                },
	            }
	        }, 
	        messages: {
	        	firstname: { 
	                required: "Please enter a First Name", 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            lastname: { 
	                required: "Please enter a Last Name", 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            password: { 
	            	required: "Please enter a Password", 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	            }, 
	            password_confirm: { 
	                minlength: jQuery.format("Enter at least {0} characters"), 
	                equalTo: "Please enter the same password as above" 
	            }, 
	            email: { 
	                required: "Please enter a valid email address", 
	                minlength: "Please enter a valid email address", 
	                remote: jQuery.format("{0} is already in use")
	            }
	        },
	        errorPlacement: function(error, element) { 
				error.appendTo( element.parent() ); 
				element.parent().parent().addClass("error");
	        }, 
	        submitHandler: function() { 
	            //alert("submitted!"); 
	        },
	        invalidHandler: function(error, element){
	        	//element.parent().parent().addClass("error");
	        },
	        showErrors: function (errorMap, errorList) {
	        	for (var i = 0; errorList[i]; i++) {
	            	var element = this.errorList[i].element;
	            	this.errorsFor(element).remove();
	        	}
	        	this.defaultShowErrors();
	    	},
	        success: function(label) { 
	            label.parent().parent().removeClass("error");
	        }
	    }); 

    	$('.alert').hide('fast');
			
		if($("#userAccountForm").valid()){
			$.ajax({
				url : URL_BASE+'/account',
				type: 'POST',
	            dataType: 'text',
				data: $('#userAccountForm').serialize(),
				beforeSend : function(){
					$('#userAccountButton').button('loading');
				},
				error : function(xhr, ajaxOptions, thrownError) {
					alert('Error ' + xhr.statusText + xhr.status + xhr.responseText);
					if(xhr.status=='420'){
						 $('.alert-error').show('fast');
					}
					$('#userAccountButton').button('reset');
				},
				success : function(msg) {
					$('#userAccountForm')[0].reset();
					$('#userAccountButton').button('reset');
					$('.alert-error').hide();
					$('.alert-success').show('fast').delay(3000).fadeOut('slow');
				}
			});
		}
		
		return false;
	});
	
});