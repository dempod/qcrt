<?php
	class admin_store extends admin{
		//*** STORES ***
		public function view(){
			$stores = new Axon('store');
			$astores = $stores->afind('site_id="'.F3::get('SESSION.site').'"', 'number ASC');
		
			F3::set('stores', $astores);
			F3::set('menu','stores');
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/stores.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/stores.js');
			F3::set('html_title','Admin - Stores');
			echo Template::serve('html/admin/layout.html');
		}
		public function create(){
			if($_POST){
				$store = new Axon('store');
				
				$store->active    = 1;
				$store->number    = $_POST['number'];
				$store->name      = $_POST['name'];
				$store->address   = $_POST['address'];
				$store->address2  = $_POST['address2'];
				$store->phone     = $_POST['phone'];
				$store->location  = $_POST['location'];
				$store->director  = $_POST['director'];
				$store->surveyor  = $_POST['surveyor'];
				$store->entrances = $_POST['entrances'];
				$store->admin_id  = F3::get('SESSION.admin');
				$store->site_id   = F3::get('SESSION.site');
				
				$store->save();
				F3::reroute('/admin/stores');
				return;
				
			}else{
				F3::set('menu','stores');
				F3::set('header','html/admin/header.html');
				F3::set('content','html/admin/stores/new.html');
				F3::set('footer','html/admin/footer.html');
				F3::set('js','js/admin/stores/new.js');
				F3::set('html_title','New Store Page');
				echo Template::serve('html/admin/layout.html');
			}
		}
		
		public function delete(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$store = new Axon('store');
				$store->load('id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				
				$folder = F3::get('SESSION.subdomain').'/store_'.$store->id.'/';
				
				$s3 = new AmazonS3();
				
				$response = $s3->get_object_list('qcrt', array(
				   'prefix' => $folder
				));

				foreach ($response as $v) {
				    $s3->delete_object('qcrt', $v);
				}
				
				$store->erase();
				
				//Delete Window Records
				$window = new Axon('window');
				$window->load('store_id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				$window->erase();
				
				//Delete Door Records
				$door = new Axon('door');
				$door->load('store_id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				$door->erase();
				
				echo $_POST['id'];
			}
		}
		
		public function update(){
			if($_POST){
				$store = new Axon('store');
				$store->load('id="'.$_POST['store_id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				
				$store->number    = $_POST['number'];
				$store->name      = $_POST['name'];
				$store->address   = $_POST['address'];
				$store->address2  = $_POST['address2'];
				$store->phone     = $_POST['phone'];
				$store->location  = $_POST['location'];
				$store->director  = $_POST['director'];
				$store->surveyor  = $_POST['surveyor'];
				$store->entrances = $_POST['entrances'];
				$store->active    = isset($_POST['status']) ? 1 : 0;
				
				$store->save();
				F3::reroute('/admin/stores');
				return;
			}else{
				$store = new Axon('store');
				$store->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				
				if($store->dry()){
					F3::reroute('/admin/stores');
				}else{
					F3::set('store', $store);
					F3::set('menu','stores');
					F3::set('header','html/admin/header.html');
					F3::set('content','html/admin/stores/edit.html');
					F3::set('footer','html/admin/footer.html');
					F3::set('js','js/admin/stores/edit.js');
					F3::set('html_title','Edit Store Page');
					echo Template::serve('html/admin/layout.html');
				}
			}
		}
		public function status(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$store = new Axon('store');
				$store->load('id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				$store->active = $_POST['status'];
				$store->save();
			}
			return;
		}
		//*** END STORES ***
	}
?>