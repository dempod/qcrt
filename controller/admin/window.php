<?php
	class admin_window extends admin{
		//*** windows ***
		public function view(){
			
			F3::set('menu','windows');
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/windows.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/windows.js');
			F3::set('html_title','Admin - Windows');
						
			if(F3::get('PARAMS["id"]')){
				$store = new Axon('store');
				$store->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				if($store->dry()){
					F3::reroute('/admin/windows');
				}else{
					F3::set('store_title', function($id){
						$store = new Axon('store');
						$store->load('id='.$id.' AND site_id="'.F3::get('SESSION.site').'"');
						return $store->name.' - #'.$store->number;
					});
					
					$window = new Axon('window');
					F3::set('windows', $window->afind('store_id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"'));
				}
			}else{
				$stores = new Axon('store');
				$astores = $stores->afind('site_id="'.F3::get('SESSION.site').'" AND site_id="'.F3::get('SESSION.site').'"', 'number ASC');
				F3::set('stores', $astores);
			}
			
			//Get Thumbnail Function			
			F3::set('img_thumb', function($key){
				$path_parts = pathinfo($key);					
				return $path_parts['dirname'].'/'.$path_parts['filename'].'_thumb.'.$path_parts['extension'];
			});
						
			echo Template::serve('html/admin/layout.html');
		}
		
		public function create(){
			if($_POST){
				$window = new Axon('window');
				
				$window->store_id        = $_POST['store_id'];
				$window->name            = $_POST['name']=='' ? $_POST['window_name'] : $_POST['name'];
				$window->type            = $_POST['name']=='' ? 'window' : 'view';
				$window->animation       = $_POST['animation'];
				$window->plinth          = $_POST['plinth'];
				$window->track_rod       = $_POST['track_rod'];
				$window->tinted          = $_POST['tinted'];
				$window->outlets         = $_POST['outlets'];
				$window->outlet_number   = $_POST['outlet_number'];
				$window->outlet_location = $_POST['outlet_location'];
				$window->pane_1_w        = $_POST['pane_1_w'];
				$window->pane_1_h        = $_POST['pane_1_h'];
				$window->pane_2_w        = $_POST['pane_2_w'];
				$window->pane_2_h        = $_POST['pane_2_h'];
				$window->pane_3_w        = $_POST['pane_3_w'];
				$window->pane_3_h        = $_POST['pane_3_h'];
				$window->pane_4_w        = $_POST['pane_4_w'];
				$window->pane_4_h        = $_POST['pane_4_h'];
				$window->pane_5_w        = $_POST['pane_5_w'];
				$window->pane_5_h        = $_POST['pane_5_h'];
				$window->pane_6_w        = $_POST['pane_6_w'];
				$window->pane_6_h        = $_POST['pane_6_h'];
				$window->image           = $_POST['image'];
				$window->notes           = $_POST['notes'];
				$window->active          = 1;
				$window->admin_id 		 = F3::get('SESSION.admin');
				$window->site_id		 = F3::get('SESSION.site');

				$window->save();
				F3::reroute('/admin/windows/'.$_POST['store_id']);
				return;
				
			}else{
				
				if(F3::get('PARAMS["id"]')){
					$store = new Axon('store');
					$store->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
					if($store->dry()){
						F3::reroute('/admin/windows');
					}else{
						F3::set('store_title', function($id){
							$store = new Axon('store');
							$store->load('id='.$id.' AND site_id="'.F3::get('SESSION.site').'"');
							return $store->name.' - #'.$store->number;
						});
					}
				}else{
					F3::reroute('/admin/windows');
				}
				
				$date = new DateTime(null, new DateTimeZone('GMT'));
				$date->modify('+24 hour');
				$expires = $date->format("Y-m-d\TH:i:s\Z");
				$aws_callback = 'http://'.F3::get('SESSION.subdomain').'.qcrt.com/admin/windows/uploadCallback/1';
				$MAX_FILE_SIZE = 50 * 1048576;
				$folder = F3::get('SESSION.subdomain').'/store_'.F3::get('PARAMS["id"]');

				$YOUR_POLICY_DOCUMENT_BASE64_ENCODED = base64_encode('{"expiration": "'.$expires.'","conditions":[{"bucket": "qcrt"},["starts-with", "$key", "'.$folder.'/"],{"acl": "public-read"},{"success_action_redirect": "'.$aws_callback.'"},["content-length-range", 0, '. $MAX_FILE_SIZE .'],["starts-with", "$Content-Type", "image/"]]}');

				$YOUR_CALCULATED_SIGNATURE = base64_encode(hash_hmac('sha1', $YOUR_POLICY_DOCUMENT_BASE64_ENCODED, 'B7ey5lcqOvEHsTJzMxDewyqvLUyZ/eVgARBx7Hxg', true));
				
				F3::set('aws_key', CFCredentials::get()->key);
				F3::set('aws_policy', $YOUR_POLICY_DOCUMENT_BASE64_ENCODED);
				F3::set('aws_signature', $YOUR_CALCULATED_SIGNATURE);
				F3::set('aws_user_bucket', 'qcrt');
				F3::set('aws_folder', $folder);
				F3::set('aws_callback', $aws_callback);				
				
				F3::set('menu','windows');
				F3::set('header','html/admin/header.html');
				F3::set('content','html/admin/windows/new.html');
				F3::set('footer','html/admin/footer.html');
				F3::set('js','js/admin/windows/new.js');
				F3::set('html_title','New Window Page');
				echo Template::serve('html/admin/layout.html');
			}
		}
		
		public function uploadCallback(){

			if(isset($_GET['key']) && $_GET['key']!=''){
				
				$file_id = uniqid();
				
				$path_parts = pathinfo($_GET['key']);
				$ext = $path_parts['extension'];
				$folder = $path_parts['dirname'];
				$new_key = $folder.'/'.$file_id.'.'.$ext;
				$thumb_key = $folder.'/'.$file_id.'_thumb.'.$ext;
				
				//Copy the object
				$s3 = new AmazonS3();
				$bucket = $_GET['bucket'];

				$response = $s3->copy_object(
				    array( // Source
				        'bucket'   => $bucket,
				        'filename' => $_GET['key']
				    ),
				    array( // Destination
				        'bucket'   => $bucket,
				        'filename' => $new_key
				    ),
					array( // Optional parameters
						'acl'  => AmazonS3::ACL_PUBLIC
					)
				);
					
				//Get Image
				$s3 = new AmazonS3();
				$url = $s3->get_object_url($bucket, $_GET['key']);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				$data = curl_exec($ch);
				curl_close($ch);
				
				$size = 150;  // new image width
			    $image_source = imagecreatefromstring($data);
			    $width = imagesx($image_source);
			    $height = imagesy($image_source);
			    $aspect_ratio = $height/$width;

			    if ($width <= $size) {
			      $new_w = $width;
			      $new_h = $height;
			    } else {
			      $new_w = $size;
			      $new_h = abs($new_w * $aspect_ratio);
			    }

			    $thumb = imagecreatetruecolor($new_w, $new_h); 

				// Copy resampled makes a smooth thumbnail
				imagecopyresampled($thumb, $image_source, 0, 0, 0, 0, $new_w, $new_h, $width, $height);
				imagedestroy($image_source);
				
				ob_start();
				imagejpeg($thumb);
				$image_contents = ob_get_clean();
		
				// Upload an object from a resource (requires size):
				$s3->create_object($bucket, $thumb_key, array(
				    'body' => $image_contents,
				    'acl' => AmazonS3::ACL_PUBLIC
				));
				
				//Delete old object
				$response = $s3->delete_object($bucket, $_GET['key']);
				
				//Return File Name
				echo $new_key;

			}else{}
		}
		
		public function delete(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$window = new Axon('window');
				$window->load('id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				
				if($window->image){
					$s3 = new AmazonS3();
					$s3->delete_object('qcrt', $window->image);
				
					$path_parts = pathinfo($window->image);					
					$thumb_key = $path_parts['dirname'].'/'.$path_parts['filename'].'_thumb.'.$path_parts['extension'];
					$s3->delete_object('qcrt', $thumb_key);
				}
				
				$window->erase();
				echo $_POST['id'];
			}
		}
		
		public function update(){
			if($_POST){
				$window = new Axon('window');
				$window->load('id="'.$_POST['window_id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				
				//Delete Old Image if Different
				if($window->image != $_POST['image'] && $window->image!=''){
					$s3 = new AmazonS3();
					$s3->delete_object('qcrt', $window->image);
				
					$path_parts = pathinfo($window->image);	
					$thumb_key = $path_parts['dirname'].'/'.$path_parts['filename'].'_thumb.'.$path_parts['extension'];
					$s3->delete_object('qcrt', $thumb_key);
				}
				
				$window->store_id        = $_POST['store_id'];
				$window->name            = $_POST['name']=='' ? $_POST['window_name'] : $_POST['name'];
				$window->type            = $_POST['name']=='' ? 'window' : 'view';
				$window->animation       = $_POST['animation'];
				$window->plinth          = $_POST['plinth'];
				$window->track_rod       = $_POST['track_rod'];
				$window->tinted          = $_POST['tinted'];
				$window->outlets         = $_POST['outlets'];
				$window->outlet_number   = $_POST['outlet_number'];
				$window->outlet_location = $_POST['outlet_location'];
				$window->pane_1_w        = $_POST['pane_1_w'];
				$window->pane_1_h        = $_POST['pane_1_h'];
				$window->pane_2_w        = $_POST['pane_2_w'];
				$window->pane_2_h        = $_POST['pane_2_h'];
				$window->pane_3_w        = $_POST['pane_3_w'];
				$window->pane_3_h        = $_POST['pane_3_h'];
				$window->pane_4_w        = $_POST['pane_4_w'];
				$window->pane_4_h        = $_POST['pane_4_h'];
				$window->pane_5_w        = $_POST['pane_5_w'];
				$window->pane_5_h        = $_POST['pane_5_h'];
				$window->pane_6_w        = $_POST['pane_6_w'];
				$window->pane_6_h        = $_POST['pane_6_h'];
				$window->image           = $_POST['image'];
				$window->notes           = $_POST['notes'];
				$window->active          = isset($_POST['status']) ? 1 : 0;

				$window->save();
				F3::reroute('/admin/windows/'.$_POST['store_id']);
				return;
			}else{
				$window = new Axon('window');
				$window->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				
				if($window->dry()){
					F3::reroute('/admin/windows');
				}else{
								
					$date = new DateTime(null, new DateTimeZone('GMT'));
					$date->modify('+24 hour');
					$expires = $date->format("Y-m-d\TH:i:s\Z");
					$aws_callback = 'http://'.F3::get('SESSION.subdomain').'.qcrt.com/admin/windows/uploadCallback/1';
					$MAX_FILE_SIZE = 50 * 1048576;
					$folder = F3::get('SESSION.subdomain').'/store_'.$window->store_id;

					$YOUR_POLICY_DOCUMENT_BASE64_ENCODED = base64_encode('{"expiration": "'.$expires.'","conditions":[{"bucket": "qcrt"},["starts-with", "$key", "'.$folder.'/"],{"acl": "public-read"},{"success_action_redirect": "'.$aws_callback.'"},["content-length-range", 0, '. $MAX_FILE_SIZE .'],["starts-with", "$Content-Type", "image/"]]}');

					$YOUR_CALCULATED_SIGNATURE = base64_encode(hash_hmac('sha1', $YOUR_POLICY_DOCUMENT_BASE64_ENCODED, 'B7ey5lcqOvEHsTJzMxDewyqvLUyZ/eVgARBx7Hxg', true));

					F3::set('aws_key', CFCredentials::get()->key);
					F3::set('aws_policy', $YOUR_POLICY_DOCUMENT_BASE64_ENCODED);
					F3::set('aws_signature', $YOUR_CALCULATED_SIGNATURE);
					F3::set('aws_user_bucket', 'qcrt');
					F3::set('aws_folder', $folder);
					F3::set('aws_callback', $aws_callback);
					
					F3::set('store_title', function($id){
						$store = new Axon('store');
						$store->load("id=$id");
						return $store->name.' - #'.$store->number;
					});
					
					//Get Thumbnail Function			
					F3::set('img_thumb', function($key){
						$path_parts = pathinfo($key);					
						return $path_parts['dirname'].'/'.$path_parts['filename'].'_thumb.'.$path_parts['extension'];
					});
					
					F3::set('window', $window);
					F3::set('menu','windows');
					F3::set('header','html/admin/header.html');
					F3::set('content','html/admin/windows/edit.html');
					F3::set('footer','html/admin/footer.html');
					F3::set('js','js/admin/windows/edit.js');
					F3::set('html_title','Edit Window Page');
					echo Template::serve('html/admin/layout.html');
				}
			}
		}
		
		public function status(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$window = new Axon('window');
				$window->load('id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				$window->active = $_POST['status'];
				$window->save();
			}
			return;
		}
		//*** END WINDOWS ***
	}
?>