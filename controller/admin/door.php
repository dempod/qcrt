<?php
	class admin_door extends admin{
		//*** DOORS ***
		public function view(){
			
			F3::set('menu','doors');
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/doors.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/doors.js');
			F3::set('html_title','Admin - Doors');
						
			if(F3::get('PARAMS["id"]')){
				$store = new Axon('store');
				$store->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				if($store->dry()){
					F3::reroute('/admin/doors');
				}else{
					F3::set('store_title', function($id){
						$store = new Axon('store');
						$store->load('id='.$id.' AND site_id="'.F3::get('SESSION.site').'"');
						return $store->name.' - #'.$store->number;
					});
					
					$door = new Axon('door');
					F3::set('doors', $door->afind('store_id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"'), 'number ASC');
				}
			}else{
				$stores = new Axon('store');
				$astores = $stores->afind('site_id="'.F3::get('SESSION.site').'"', 'number ASC');
				F3::set('stores', $astores);
			}
						
			echo Template::serve('html/admin/layout.html');
		}
		
		public function create(){
			if($_POST){
				$door = new Axon('door');
				
				$door->store_id = $_POST['store_id'];
				$door->active   = 1;
				$door->admin_id = F3::get('SESSION.admin');
				$door->name     = $_POST['name'];
				$door->entry    = $_POST['entry'];
				$door->height   = $_POST['height'];
				$door->width    = $_POST['width'];
				$door->note     = $_POST['note'];
				$door->site_id  = F3::get('SESSION.site');


				$door->save();
				F3::reroute('/admin/doors/'.$_POST['store_id']);
				return;
				
			}else{
				
				if(F3::get('PARAMS["id"]')){
					$store = new Axon('store');
					$store->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
					if($store->dry()){
						F3::reroute('/admin/doors');
					}else{
						F3::set('store_title', function($id){
							$store = new Axon('store');
							$store->load('id='.$id.' AND site_id="'.F3::get('SESSION.site').'"');
							return $store->name.' - #'.$store->number;
						});
					}
				}else{
					F3::reroute('/admin/doors');
				}			
				
				F3::set('menu','doors');
				F3::set('header','html/admin/header.html');
				F3::set('content','html/admin/doors/new.html');
				F3::set('footer','html/admin/footer.html');
				F3::set('js','js/admin/doors/new.js');
				F3::set('html_title','New Window Page');
				echo Template::serve('html/admin/layout.html');
			}
		}
		
		public function delete(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$door = new Axon('door');
				$door->load('id="'.$_POST['id'].'"');		
				$door->erase();
				echo $_POST['id'];
			}
		}
		
		public function update(){
			if($_POST){
				$door = new Axon('door');
				$door->load('id="'.$_POST['door_id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				
				$door->store_id = $_POST['store_id'];
				$door->active   = isset($_POST['status']) ? 1 : 0;;
				$door->name     = $_POST['name'];
				$door->entry    = $_POST['entry'];
				$door->height   = $_POST['height'];
				$door->width    = $_POST['width'];
				$door->note     = $_POST['note'];

				$door->save();
				F3::reroute('/admin/doors/'.$_POST['store_id']);
				return;
			}else{
				$door = new Axon('door');
				$door->load('id="'.F3::get('PARAMS["id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				
				if($door->dry()){
					F3::reroute('/admin/doors');
				}else{
					
					F3::set('store_title', function($id){
						$store = new Axon('store');
						$store->load('id='.$id.' AND site_id="'.F3::get('SESSION.site').'"');
						return $store->name.' - #'.$store->number;
					});
					
					F3::set('door', $door);
					F3::set('menu','doors');
					F3::set('header','html/admin/header.html');
					F3::set('content','html/admin/doors/edit.html');
					F3::set('footer','html/admin/footer.html');
					F3::set('js','js/admin/doors/edit.js');
					F3::set('html_title','Edit Window Page');
					echo Template::serve('html/admin/layout.html');
				}
			}
		}
		
		public function status(){
			if(isset($_POST['id']) && $_POST['id']!=''){
				$door = new Axon('door');
				$door->load('id="'.$_POST['id'].'" AND site_id="'.F3::get('SESSION.site').'"');
				$door->active = $_POST['status'];
				$door->save();
			}
			return;
		}
		//*** END DOORS ***
	}
?>