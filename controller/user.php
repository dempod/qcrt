<?php
	class user{
	
		function beforeRoute(){
			//Check Authentication
			if (!F3::get('SESSION.user') && F3::get('PARAMS[0]')!='/user/login' && F3::get('PARAMS[0]')!='/user/forgot'){
				F3::reroute('/user/login');
			}
			
			if(F3::get('SESSION.user') && F3::get('PARAMS[0]')!='/user/login' && F3::get('PARAMS[0]')!='/user/forgot'){
				$user = new Axon('user');
				$user->load('id="'.F3::get('SESSION.user').'"');
				
				if($user->active != 1){
					$this->logout();
				}	
			}
		}
		
		public function home(){
			F3::set('menu','home');
			F3::set('header','html/user/header.html');
			F3::set('content','html/user/home.html');
			F3::set('footer','html/user/footer.html');
			F3::set('js','js/user/home.js');
			F3::set('html_title','User Home Page');
			
			$stores = new Axon('store');
			$astores = $stores->afind('active=1 AND site_id="'.F3::get('SESSION.site').'"');
			F3::set('stores', $astores);
			
			if(F3::get('PARAMS["store_id"]')){
				$my_store = new Axon('store');
				$my_store->load('id="'.F3::get('PARAMS["store_id"]').'" AND site_id="'.F3::get('SESSION.site').'"');
				if($my_store->dry() || $my_store->active!=1){
					F3::reroute('/user');
				}else{
					F3::set('store', $my_store);
					
					$window = new Axon('window');
					F3::set('windows', $window->afind('store_id="'.F3::get('PARAMS["store_id"]').'" AND type="window" AND active="1"'));
					
					$view = new Axon('window');
					F3::set('views', $view->afind('store_id="'.F3::get('PARAMS["store_id"]').'" AND type="view" AND active="1"'));
					
					$door = new Axon('door');
					$door->afind('store_id="'.F3::get('PARAMS["store_id"]').'"');
					F3::set('doors', $door->afind('store_id="'.F3::get('PARAMS["store_id"]').'" AND active="1"'));
				}
			}
			
			//Get Thumbnail Function			
			F3::set('img_thumb', function($key){
				$path_parts = pathinfo($key);					
				return $path_parts['dirname'].'/'.$path_parts['filename'].'_thumb.'.$path_parts['extension'];
			});
			
			echo Template::serve('html/user/layout.html');
		}
		
		public function account(){
			if($_POST){
				$user = new Axon('user');
				$user->load('id="'.F3::get('SESSION.user').'"');
			
				$user->email = $_POST['email'];
				$user->firstname = $_POST['firstname'];
				$user->lastname = $_POST['lastname'];
			
				if($_POST['password']!=''){
					$user->password = sha1($_POST['password']);
				}
			
				$user->save();
			}else{	
				$user = new Axon('user');
				$user->load('id="'.F3::get('SESSION.user').'"');
			
				F3::set('user', $user);
			
				F3::set('menu','account');
				F3::set('header','html/user/header.html');
				F3::set('content','html/user/account.html');
				F3::set('footer','html/user/footer.html');
				F3::set('js','js/user/account.js');
				F3::set('html_title','User Account Page');
				echo Template::serve('html/user/layout.html');
			}
		}
		
		public function checkUserEmail(){
			$user = new Axon('user');
			$user->load('email="'.$_POST['email'].'"');


			if($user->dry()){
				echo "true";
			}else{
				if($user->id==$_POST['user_id']){
					echo "true";
				}else{
					echo "false";
				}
			}
		}
		
		public function login(){
			if($_POST){
				$email = $_POST['email'];
				$password = $_POST['password'];
				
				F3::set('AUTH', array('table'=>'user','id'=>'email','pw'=>'password'));
				$auth = Auth::sql($email, sha1($password));
				
				$sites = new Axon('site');
				$sites->load('active="1" AND subdomain="'.F3::get('subdomain').'"');
				
				if ($auth && $auth->active == 1 && ($auth->site_id == $sites->id || $auth->site_id == 0)) {		
					F3::set('SESSION.token', $auth->user_token);
		  			F3::set('SESSION.user', $auth->id);
					F3::set('SESSION.site', $sites->id);
					return;
				} else {
					//User is not authenticated - send error
					header('HTTP/1.1 420 Invalid Login Credentials');
					return;
				}
			}else{
				F3::set('header','html/public/header.html');
				F3::set('content','html/user/login.html');
				F3::set('footer','html/public/footer.html');
				F3::set('js','js/user/login.js');
				F3::set('html_title','User Sign In');
				echo Template::serve('html/layout.html');
			}
		}
		
		public function logout(){
			if (F3::get('SESSION.user')){
				F3::set('SESSION.user', null);
			}
			if (F3::get('SESSION.token')){
				F3::set('SESSION.token', null);
			}
			F3::reroute('/');
		}
		
		public function forgot(){
			if($_POST){
				$email = $_POST['email'];
				$user = new Axon('user');
				$user->load('email="'.$email.'"');
				
				if ($user->email) {
					
					$arr = str_split('abcdefghkABCDEFGHK23456789'); // get all the characters into an array
    				shuffle($arr); // randomize the array
    				$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    				$tempPw = implode('', $arr); // smush them back into a string
    				
    				$user->password = sha1($tempPw);
					
					$mail=new SMTP('mail.directedgemedia.com',465,'SSL','jake@directedgemedia.com','myers478');
					$mail->set('from','<support@qcrt.com>');
					$mail->set('reply-to', 'support@qcrt.com');
					$mail->set('x-mailer', 'PHP/' . phpversion());
					$mail->set('to', $admin->email);
					$mail->set('subject','Your Temporary Password');
					$mail->send("Here is your temporary password: ".$tempPw."\n\nPlease log in and update your password.");
					
					$user->save();
					return;
					
				} else {
					//User is not authenticated - send error
					header('HTTP/1.1 420 Email Not Found');
					return;
				}
				
			}else{
				F3::set('header','html/public/header.html');
				F3::set('content','html/user/forgot.html');
				F3::set('footer','html/public/footer.html');
				F3::set('js','js/user/forgot.js');
				F3::set('html_title','Forgot your password?');
				echo Template::serve('html/layout.html');
			}
		}
		
		function afterRoute(){}
	}
?>