<?php
	class admin{
	
		function beforeRoute(){
			//Check Authentication
			if (!F3::get('SESSION.admin') && F3::get('PARAMS[0]')!='/admin/login' && F3::get('PARAMS[0]')!='/admin/forgot'){
				F3::reroute('/admin/login');
			}
			
			if($_SERVER['HTTP_HOST'] != 'www.qcrt.com'){
				//F3::reroute('http://www.qcrt.com'.$_SERVER['REQUEST_URI']);
			}
		}
		
		public function login(){
			if($_POST){
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				$sites = new Axon('site');
				$sites->load('active="1" AND subdomain="'.F3::get('subdomain').'"');
				
				F3::set('AUTH', array('table'=>'admin','id'=>'username','pw'=>'password'));
				$auth = Auth::sql($username, sha1($password));
				if ($auth && $auth->active=='1' && !$sites->dry()) {
					F3::set('SESSION.site', $sites->id);
					F3::set('SESSION.subdomain', $sites->subdomain);
		  			F3::set('SESSION.admin', $auth->id);
					return;
				} else {
					//User is not authenticated - send error
					header('HTTP/1.1 420 Invalid Login Credentials');
					return;
				}
			}else{
				F3::set('header','html/public/header.html');
				F3::set('content','html/admin/login.html');
				F3::set('footer','html/public/footer.html');
				F3::set('js','js/admin/login.js');
				F3::set('html_title','Administrator Sign In');
				echo Template::serve('html/layout.html');
			}
		}
		
		public function logout(){
			if (F3::get('SESSION.admin')){
				F3::set('SESSION.admin', null);
			}
			F3::reroute(F3::get('URL_BASE'));
		}
		
		public function forgot(){
			if($_POST){
				$email = $_POST['email'];
				$admin = new Axon('admin');
				$admin->load('email="'.$email.'"');
				
				if ($admin->email) {
					
					$arr = str_split('abcdefghkABCDEFGHK23456789'); // get all the characters into an array
    				shuffle($arr); // randomize the array
    				$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    				$tempPw = implode('', $arr); // smush them back into a string
    				
    				$admin->password = sha1($tempPw);
						
					$mail=new SMTP('mail.directedgemedia.com',465,'SSL','jake@directedgemedia.com','myers478');
					$mail->set('from','<support@qcrt.com>');
					$mail->set('reply-to', 'support@qcrt.com');
					$mail->set('x-mailer', 'PHP/' . phpversion());
					$mail->set('to', $admin->email);
					$mail->set('subject','Your Username and Temporary Password');
					$mail->send("Here is your username and temporary password:\n\nUsername: ".$admin->username."\nPassword: ".$tempPw."\n\nPlease log in and update your password.");
					
					$admin->save();
					return;
					
				} else {
					//User is not authenticated - send error
					header('HTTP/1.1 420 Email Not Found');
					return;
				}
				
			}else{
				F3::set('header','html/public/header.html');
				F3::set('content','html/admin/forgot.html');
				F3::set('footer','html/public/footer.html');
				F3::set('js','js/admin/forgot.js');
				F3::set('html_title','Forgot your username or password?');
				echo Template::serve('html/layout.html');
			}
		}		
		
		public function home(){
			F3::set('menu','home');
			F3::set('header','html/admin/header.html');
			
			$admin = new Axon('admin');
			$admin->load('id="'.F3::get('SESSION.admin').'"');
			
			F3::set('admin', $admin);
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/home.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/home.js');
			
			F3::set('html_title','Admin Page');
			echo Template::serve('html/admin/layout.html');
		}
		
		public function account(){
			$admin = new Axon('admin');
			$admin->load('id="'.F3::get('SESSION.admin').'"');
		
			F3::set('menu','account');
			F3::set('admin', $admin);
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/account.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/account.js');
			F3::set('html_title','Admin Page');
			echo Template::serve('html/admin/layout.html');
		}
		
		public function users(){
			$users = new Axon('user');
			$ausers = $users->afind('site_id="'.F3::get('SESSION.site').'" OR site_id=0');
		
			F3::set('users', $ausers);
			F3::set('menu','users');
			F3::set('header','html/admin/header.html');
			F3::set('content','html/admin/users.html');
			F3::set('footer','html/admin/footer.html');
			F3::set('js','js/admin/users.js');
			F3::set('html_title','Admin Page');
			echo Template::serve('html/admin/layout.html');
		}
		
		public function change_status(){
		
			if(isset($_POST['user_id']) && $_POST['user_id']!=''){
				$user = new Axon('user');
				$user->load('id="'.$_POST['user_id'].'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');
				$user->active = $_POST['status'];
				$user->save();
			}
			return;
		}
		
		public function edit_user(){
			if($_POST){
				$user = new Axon('user');
				$user->load('id="'.$_POST['user_id'].'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');
				$user->firstname = $_POST['firstname'];
				$user->lastname  = $_POST['lastname'];
				$user->email     = $_POST['email'];
				$user->active    = isset($_POST['status']) ? 1 : 0;
				$user->site_id   = isset($_POST['is_global']) ? $_POST['is_global'] : F3::get('SESSION.site');

				
				if(isset($_POST['auto_password'])){
					$arr = str_split('abcdefghkABCDEFGHK23456789'); // get all the characters into an array
    				shuffle($arr); // randomize the array
    				$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    				$tempPw = implode('', $arr); // smush them back into a string
    				$user->password = sha1($tempPw);
    				
    				//Send Temporary Password In Email		
					$mail=new SMTP('mail.directedgemedia.com',465,'SSL','jake@directedgemedia.com','myers478');
					$mail->set('from','<support@qcrt.com>');
					$mail->set('reply-to', 'support@qcrt.com');
					$mail->set('x-mailer', 'PHP/' . phpversion());
					$mail->set('to', $_POST['email']);
					$mail->set('subject','Your Temporary Password');
					$mail->send("Here is your temporary password:\n\Email: ".$_POST['email']."\nPassword: ".$tempPw."\n\nPlease log in and update your password.");
    				
				}else if($_POST['password']!='' && $_POST['password_confirm']!='' && $_POST['password']==$_POST['password_confirm']){
					$user->password = sha1($_POST['password']);
				}
				
				$user->save();
				F3::reroute('/admin/users');
				return;
			}else{
				$user = new Axon('user');
				$user->load('id="'.F3::get('PARAMS["id"]').'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');
				
				if($user->dry()){
					F3::reroute('/admin/users');
				}else{
					F3::set('user', $user);
					F3::set('menu','users');
					F3::set('header','html/admin/header.html');
					F3::set('content','html/admin/users/edit.html');
					F3::set('footer','html/admin/footer.html');
					F3::set('js','js/admin/users/edit.js');
					F3::set('html_title','Edit User Page');
					echo Template::serve('html/admin/layout.html');
				}
			}
		}
		
		public function new_user(){
			if($_POST){
				$user = new Axon('user');
				$user->firstname = $_POST['firstname'];
				$user->lastname  = $_POST['lastname'];
				$user->email     = $_POST['email'];
				$user->active    = 1;
				$user->admin_id  = F3::get('SESSION.admin');
				$user->site_id   = isset($_POST['is_global']) ? $_POST['is_global'] : F3::get('SESSION.site');
				
				if(isset($_POST['auto_password'])){
					$arr = str_split('abcdefghkABCDEFGHK23456789'); // get all the characters into an array
    				shuffle($arr); // randomize the array
    				$arr = array_slice($arr, 0, 6); // get the first six (random) characters out
    				$tempPw = implode('', $arr); // smush them back into a string
    				$user->password = sha1($tempPw);
    				    				
    				//Send Temporary Password In Email
					$mail=new SMTP('mail.directedgemedia.com',465,'SSL','jake@directedgemedia.com','myers478');
					$mail->set('from','<support@qcrt.com>');
					$mail->set('reply-to', 'support@qcrt.com');
					$mail->set('x-mailer', 'PHP/' . phpversion());
					$mail->set('to', $_POST['email']);
					$mail->set('subject','Your Temporary Password');
					$mail->send("Here is your temporary password:\n\Email: ".$_POST['email']."\nPassword: ".$tempPw."\n\nPlease log in and update your password.");
					
				}else{
					$user->password = sha1($_POST['password']);
				}
				
				if(isset($_POST['welcome_email'])){
    				    				
    				//Send Welcome Email				
					$mail=new SMTP('mail.directedgemedia.com',465,'SSL','jake@directedgemedia.com','myers478');
					$mail->set('from','<support@qcrt.com>');
					$mail->set('reply-to', 'support@qcrt.com');
					$mail->set('x-mailer', 'PHP/' . phpversion());
					$mail->set('to', $_POST['email']);
					$mail->set('subject','Welcome to QCRT');
					$mail->send("Welcome email from QCRT.com");
					
				}
				
				$user->save();
				F3::reroute('/admin/users');
				return;
				
			}else{
				F3::set('menu','users');
				F3::set('header','html/admin/header.html');
				F3::set('content','html/admin/users/new.html');
				F3::set('footer','html/admin/footer.html');
				F3::set('js','js/admin/users/new.js');
				F3::set('html_title','New User Page');
				echo Template::serve('html/admin/layout.html');
			}
		}
		
		public function delete_user(){
			if(isset($_POST['user_id']) && $_POST['user_id']!=''){
				$user = new Axon('user');
				$user->load('id="'.$_POST['user_id'].'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');
				$user->erase();
				
				echo $_POST['user_id'];
			}
		}
		
		public function checkNewUserEmail(){
			$user = new Axon('user');
			$user->load('email="'.$_POST['email'].'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');


			if($user->dry()){
				echo "true";
			}else{
				echo "false";
			}
		}
		
		public function checkEditUserEmail(){
			$user = new Axon('user');
			$user->load('email="'.$_POST['email'].'" AND (site_id="'.F3::get('SESSION.site').'" OR site_id=0)');


			if($user->dry()){
				echo "true";
			}else{
				if($user->id==$_POST['user_id']){
					echo "true";
				}else{
					echo "false";
				}
			}
		}
		
		public function update(){
			$admin = new Axon('admin');
			$admin->load('id="'.F3::get('SESSION.admin').'"');
			
			$admin->email     = $_POST['email'];
			$admin->username  = $_POST['username'];
			$admin->firstname = $_POST['firstname'];
			$admin->lastname  = $_POST['lastname'];

			
			if($_POST['password']!=''){
				$admin->password = sha1($_POST['password']);
			}
			
			$admin->save();
			return;
		}
		
		function afterRoute(){}
			
	}
?>