<?php
	class front{
		function beforeRoute(){}
		
		public function home(){
			F3::set('header','html/public/header.html');
			F3::set('footer','html/public/footer.html');
			
			if(strtok($_SERVER["HTTP_HOST"], ".")=='www'){
				$sites = new Axon('site');

				F3::set('sites', $sites->afind('active="1"'));
				F3::set('content', 'html/public/home.html');
			}else{
				F3::set('content','html/user/login.html');
				F3::set('js','js/user/login.js');
			}
			
			F3::set('html_title','Home Page');
			echo Template::serve('html/layout.html');
		}
		
		public function account(){
			if($_POST){
				$user = new Axon('user');
				$user->firstname = $_POST['firstname'];
				$user->lastname = $_POST['lastname'];
				$user->email = $_POST['email'];
				$user->password = sha1($_POST['password']);
				$user->save();
				return;
			}else{
				F3::set('header','html/public/header.html');
				F3::set('content','html/public/account.html');
				F3::set('footer','html/public/footer.html');
				F3::set('js','js/public/account.js');
				F3::set('html_title','Account Sign-up Page');
				echo Template::serve('html/layout.html');
			}
		}
		
		public function checkUserEmail(){
			$user = new Axon('user');
			$user->load('email="'.$_POST['email'].'"');


			if($user->dry()){
				echo "true";
			}else{
				echo "false";
			}
		}
		
		public function about(){
			F3::set('header','html/public/header.html');
			F3::set('content','html/public/about.html');
			F3::set('footer','html/public/footer.html');
			F3::set('html_title','How It Works');
			echo Template::serve('html/layout.html');
		}
		
		public function privacy(){
			F3::set('header','html/public/header.html');
			F3::set('content','html/public/privacy.html');
			F3::set('footer','html/public/footer.html');
			F3::set('html_title','Privacy Policy');
			echo Template::serve('html/layout.html');
		}
		
		public function legal(){
			F3::set('header','html/public/header.html');
			F3::set('content','html/public/legal.html');
			F3::set('footer','html/public/footer.html');
			F3::set('html_title','Legal Notices');
			echo Template::serve('html/layout.html');
		}
		
		function afterRoute(){}
	}
?>